import ROOT as r
import sys
from array import array
from optparse import OptionParser
import importlib
import os
import glob
import re
import time

r.gInterpreter.Declare("""
    const UInt_t barWidth = 60;
    ULong64_t processed = 0, totalEvents = 0;
    std::string progressBar;
    std::mutex barMutex;
    auto registerEvents = [](ULong64_t nIncrement) {totalEvents += nIncrement;};

    ROOT::RDF::RResultPtr<ULong64_t> AddProgressBar(ROOT::RDF::RNode df, int everyN=10000, int totalN=100000) {
        processed = 0;
        totalEvents = 0;
        registerEvents(totalN);
        auto c = df.Count();
        c.OnPartialResultSlot(everyN, [everyN] (unsigned int slot, ULong64_t &cnt){
            std::lock_guard<std::mutex> l(barMutex);
            processed += everyN; //everyN captured by value for this lambda
            progressBar = "[";
            for(UInt_t i = 0; i < static_cast<UInt_t>(static_cast<Float_t>(processed)/totalEvents*barWidth); ++i){
                progressBar.push_back('|');
            }
            // escape the '\' when defined in python string
            std::cout << "\\r" << std::left << std::setw(barWidth) << progressBar << "] " << processed << "/" << totalEvents << std::flush;
        });
        return c;
    }
""")


def load_functions(function_list):
    try:
        for func in function_list:
            r.gROOT.ProcessLineSync(".L " + func + "+")
    except:
        print("No functions defined.")


def load_config(config_file_name):
    try:
        config_module = importlib.import_module(config_file_name)
        config = config_module.config
    except:
        print("Invalid config name! Please enter a valid config file name")
        sys.exit()

    return config


def rootify_branch_list(branch_list):
    r_branch_list = r.vector("string")()
    [r_branch_list.push_back(b) for b in branch_list]
    return r_branch_list


# def get_file_size(path):
#     size = 0
#     for file in glob.glob(path):
#         size += os.path.getsize(file)
#     return size


def print_list(list_to_print):
    for l in list_to_print:
        print(l)


def build_list_from_regex(tree, regex):
    branch_list = []
    branches = tree.GetColumnNames()
    for branch in branches:
        if re.search(regex, branch):
            branch_list.append(branch)
    return branch_list


def prepare_branch_list(tree, branch_list):
    if branch_list[0] == "*":
        return [None]
    final_branch_list = list(branch_list)
    for branch in branch_list:
        if "*" in branch:
            branch_regex = branch.replace("*", ".*")
            branch_regex = "^" + branch_regex
            branch_regex = branch_regex + "$"
            regex_branches = build_list_from_regex(tree, branch_regex)
            final_branch_list.remove(branch)
            final_branch_list += regex_branches

    r_final_branch_list = rootify_branch_list(final_branch_list)
    return r_final_branch_list

def define_variables(df, new_vars):
    if not new_vars[0] is None:
        for i, var in enumerate(new_vars):
            df = df.Define(var[0], var[1])
        
    return df
    
def appendExpr(exp0,exp1,op):
    if exp1 != "" and exp0 != "":
        exp0 += " " + op + " "
        exp0 += exp1
    elif exp1 != "" and exp0 == "":
        exp0 = exp1

def appendList(list0,list1):
    if list0 != None and list1 != None:
        list0 += list1
    elif list0 == None:
        list0 = list1

r.gROOT.SetBatch(True)
r.ROOT.EnableImplicitMT()

if __name__ == "__main__":
    
    parser = OptionParser()

    parser.add_option("-f", "--file", dest="file_name", help="Input file name (or list of files, separated by comma - no white spaces!)")
    parser.add_option("-t", "--tree", dest="tree_name", help="Tree name")
    parser.add_option("-p", "--path", dest="output_file_path", help="Output file path", default="") # useful to split from file name in case of regions
    parser.add_option("-o", "--output", dest="output_file_name", help="Output file name")
    parser.add_option("-c", "--config", dest="config_file_name", help="Config file name without extension", default=None)
    parser.add_option("-e", "--events", dest="n_events", help="n_events", default=0)
    parser.add_option("-s", "--sample_type", dest="sampleType", help="Type of sample (data, ttbar, bkg, signal, ttbar_alt, bkg_alt)", default="ttbar")
    parser.add_option("-d", "--debug", dest="debug", help="Debug level (0, 1)", default="0")
    parser.add_option("-m", "--multithread", dest="mt", help="Multi-thread enabled or not (0, 1)", default="1")
    parser.add_option("-l", "--lazy", dest="lazy", help="Enable lazy processing (0, 1)", default="1")
    (options, args) = parser.parse_args()

    if int(options.mt) == 0:
        r.ROOT.DisableImplicitMT()
    
    config = load_config(options.config_file_name)
    load_functions(config["functions"])

    weight = config["weight"]
    cut = config["cuts"]
    branch_list = config["branches"]
    new_vars = config["new_vars"]
    
    # append weights, cuts, branches and new vars depending on type
    if (options.sampleType == "ttbar"):
        appendExpr(weight,config["weight_ttbar"],"*")
        appendExpr(cut,config["cuts_ttbar"],"&&")
        appendList(branch_list,config["branches_ttbar"])
        appendList(new_vars,config["new_vars_ttbar"])
    if (options.sampleType == "ttbar_alt"):
        appendExpr(weight,config["weight_ttbar_alt"],"*")
        appendExpr(cut,config["cuts_ttbar_alt"],"&&")
        appendList(branch_list,config["branches_ttbar_alt"])
        appendList(new_vars,config["new_vars_ttbar_alt"])
    if (options.sampleType == "bkg"):
        appendExpr(weight,config["weight_bkg"],"*")
        appendExpr(cut,config["cuts_bkg"],"&&")
        appendList(branch_list,config["branches_bkg"])
        appendList(new_vars,config["new_vars_bkg"])
    if (options.sampleType == "bkg_alt"):
        appendExpr(weight,config["weight_bkg_alt"],"*")
        appendExpr(cut,config["cuts_bkg_alt"],"&&")
        appendList(branch_list,config["branches_bkg_alt"])
        appendList(new_vars,config["new_vars_bkg_alt"])
    if (options.sampleType == "signal"):
        appendExpr(weight,config["weight_signal"],"*")
        appendExpr(cut,config["cuts_signal"],"&&")
        appendList(branch_list,config["branches_signal"])
        appendList(new_vars,config["new_vars_signal"])
    if (options.sampleType == "data"):
        weight = "1"
        
    # append syst weights (and new_vars definitions) to proper samples
    if (options.sampleType == "ttbar" or options.sampleType == "bkg" or options.sampleType == "signal"):
        appendList(branch_list,config["branches_weights"])
        appendList(new_vars,config["new_vars_weights"])

    if int(options.debug) > 0:
        print("Slimming {0}".format(options.file_name))
        print("Applying cut {0}".format(cut))
        print("Branches to keep:")
        print_list(branch_list)
    
    if int(options.n_events) > 0:
        r.ROOT.DisableImplicitMT() #Cannot run over range in MT
    
    start = time.time()
    
    # creating list of input files
    in_file_vec = r.std.vector('string')()
    in_file_list = []
    for f in options.file_name.split(','):
        for ff in glob.glob(f):
            in_file_list.append(ff)
    # in_file_list = options.file_name.split(',')
        
    if int(options.debug) > 0:
        print("List of files to run on:",in_file_list)
        
    # for f in in_file_list:
    #     in_file_vec.push_back(f)
    # df = r.RDataFrame(options.tree_name, in_file_vec)
    
    # passing TChain (trying to fix a crash)
    in_chain = r.TChain(options.tree_name)
    for f in in_file_list:
        in_chain.Add(f)
    df = r.RDataFrame(in_chain)
    
    if int(options.n_events) > 0:
        df = df.Range(int(options.n_events))
    
    # add progress bar
    rdf_events = (df.Count()).GetValue()
    if int(options.lazy) > 0:
        count = r.AddProgressBar(r.RDF.AsRNode(df),max(100, int(rdf_events/5000)), int(rdf_events))
    
    print(rdf_events)
    
    df = define_variables(df, new_vars)
    df = df.Define("weight", weight)
    
    df = df.Filter(cut)
    
    # make Snapshots "lazy", so that event loop not triggered (can output one tree per region and run only once)
    rso = r.RDF.RSnapshotOptions()
    if int(options.lazy) > 0:
        rso.fLazy = True
    # rso.fAutoFlush = -3000000
    # rso.fAutoFlush = 1000
    # rso.fSplitLevel = 2
    # rso.fCompressionLevel = 0
    
    # list to store results of Snapshots
    results = []
    # loop on regions
    for region in config["regions"]:
        if int(options.debug) > 0:
            print("Defining region",region[0],"with additional selection",region[1])
        # tricky way of setting file and directory name based on regions...
        out_file_name = options.output_file_name
        out_tree_name = options.tree_name
        if options.output_file_path != "":
            if region[0] != "":
                out_file_name = options.output_file_path+"/"+region[0]+"/"+out_file_name
            else:
                out_file_name = options.output_file_path+"/"+out_file_name
        else:
            if region[0] != "":
                if "/" in out_file_name:
                    # replace last occurrence of "/" with "/<region-name>/
                    ss = "/"+region[0]+"/"
                    out_file_name = ss.join(out_file_name.rsplit("/", 1))
                else:
                    out_file_name = region[0]+"/"+out_file_name
        # delete existing file (to try to avoid crashes)
        os.system("rm "+out_file_name+" 2> /dev/null")
        # if needed, create directory structure
        os.system("mkdir -p `dirname "+out_file_name+"`")
        # prepare branch list
        r_branch_list = prepare_branch_list(df, branch_list)
        if int(options.lazy) <= 0:
            print("Processing region",region[0]+":")
            count = r.AddProgressBar(r.RDF.AsRNode(df),max(100, int(rdf_events/5000)), int(rdf_events))
        # for each region add the selection to the primary filter and define Snapshot
        if r_branch_list[0] is None:
            results.append(df.Filter(region[1]).Snapshot(out_tree_name, out_file_name, "", rso))
        else:
            results.append(df.Filter(region[1]).Snapshot(out_tree_name, out_file_name, r_branch_list, rso))
        if int(options.lazy) <= 0:
            print()
        
    # trigger event loop here (if lazy option was set)
    if int(options.lazy) > 0:
        print("Processing all regions:")
        df.Count().GetValue()
        print()
    
    end = time.time()
    print("Done. Time taken {0}s".format(end-start))
