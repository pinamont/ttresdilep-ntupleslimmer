config = {
    
    # branches common to all samples
    "branches": [
        "weight",
        "Mllbb",
        "met_met",
        "DeltaPhi",
        "DeltaEta",
        "lep1_pt",
        "lep2_pt",
        "lep1_dRjetMin",
        "lep2_dRjetMin",
        "PTll",
        "PTllbb",
        "jet_pt",
        "lep1_flav",
        "lep2_flav",
        "jet1_pt",
        "jet2_pt",
        "nJets",
        "nBTags_77",
        "mlb1a",
        "mlb1b",
        "mlb2a",
        "mlb2b",
    ],

    # nominal ttbar
    "branches_ttbar": [
        "isFake_el",
        "isFake_mu",
        "TopPt",
        "AntiTopPt",
        "ttMass",
        "ttPt",
        "DeltaPhi_truth",
        "isRealDilep",
    ],
    
    # alternative ttbar
    "branches_ttbar_alt": [
        "isFake_el",
        "isFake_mu",
        "TopPt",
        "AntiTopPt",
        "ttMass",
        "ttPt",
        "DeltaPhi_truth",
        "isRealDilep",
    ],
    
    # non-ttbar bkg, nominal
    "branches_bkg": [
        "isHF",
        "isFake_el",
        "isFake_mu",
        "isRealDilep",
    ],
    
    # alternative non-ttbar
    "branches_bkg_alt": [
        "isHF",
        "isFake_el",
        "isFake_mu",
        "isRealDilep",
    ],
    
    # signals
    "branches_signal": [
        "isFake_el",
        "isFake_mu",
        "isRealDilep",
    ],
    
    # weights
    # (to be added to all MC samples - not to alternative ones)
    "branches_weights": [
        "weight_muRup","weight_muFup","weight_muRdown","weight_muFdown","weight_a14_up","weight_a14_down","weight_FSRup","weight_FSRdown",
        "weight_pdf0","weight_pdf1","weight_pdf2","weight_pdf3","weight_pdf4","weight_pdf5","weight_pdf6","weight_pdf7","weight_pdf8","weight_pdf9","weight_pdf10","weight_pdf11","weight_pdf12","weight_pdf13","weight_pdf14","weight_pdf15","weight_pdf16","weight_pdf17","weight_pdf18","weight_pdf19","weight_pdf20","weight_pdf21","weight_pdf22","weight_pdf23","weight_pdf24","weight_pdf25","weight_pdf26","weight_pdf27","weight_pdf28","weight_pdf29",
        "weight_full_pileup_up","weight_full_pileup_down","weight_pileup_up","weight_pileup_down","weight_jvt_up","weight_jvt_down",
        "weight_elTrigger_up","weight_elTrigger_down","weight_elReco_up","weight_elReco_down","weight_elId_up","weight_elId_down","weight_elIso_up","weight_elIso_down","weight_muTrigger_stat_up","weight_muTrigger_stat_down","weight_muTTVA_stat_up","weight_muTTVA_stat_down","weight_muId_stat_up","weight_muId_stat_down","weight_muIso_stat_up","weight_muIso_stat_down","weight_muTrigger_syst_up","weight_muTrigger_syst_down","weight_muTTVA_syst_up","weight_muTTVA_syst_down","weight_muId_syst_up","weight_muId_syst_down","weight_muIso_syst_up","weight_muIso_syst_down",
        "has_1elCloseToJet","has_2elCloseToJet",
        "weight_btag_b0_up","weight_btag_b1_up","weight_btag_b2_up","weight_btag_b3_up","weight_btag_b4_up","weight_btag_b5_up","weight_btag_b6_up","weight_btag_b7_up","weight_btag_b8_up","weight_btag_c0_up","weight_btag_c1_up","weight_btag_c2_up","weight_btag_c3_up","weight_btag_light0_up","weight_btag_light1_up","weight_btag_light2_up","weight_btag_light3_up","weight_btag_extrap_up","weight_btag_extrap_c_up","weight_btag_b0_down","weight_btag_b1_down","weight_btag_b2_down","weight_btag_b3_down","weight_btag_b4_down","weight_btag_b5_down","weight_btag_b6_down","weight_btag_b7_down","weight_btag_b8_down","weight_btag_c0_down","weight_btag_c1_down","weight_btag_c2_down","weight_btag_c3_down","weight_btag_light0_down","weight_btag_light1_down","weight_btag_light2_down","weight_btag_light3_down","weight_btag_extrap_down","weight_btag_extrap_c_down",
    ],


    # cuts to apply
    # (including cut at 30 GeV for lepton pT)
    "cuts": "lep2_pt>30e3 && nBTags_77>=1 && ((el_pt.size()==1 && mu_pt.size()==1) || (Mll>15e3 && (Mll<81e3 || Mll>101e3) && met_met>45e3)) && isOS && ((mlb1a<150e3 && mlb2a<150e3) || (mlb1b<150e3 && mlb2b<150e3))",
    "cuts_ttbar": "",
    "cuts_ttbar_alt": "",
    "cuts_bkg": "",
    "cuts_bkg_alt": "",
    "cuts_signal": "",
    
    "regions": [
        # ["VR1","(DeltaPhi/TMath::Pi())>0 && (DeltaPhi/TMath::Pi())<=0.5    && ((Mllbb/1e3<450 && DeltaEta>1) || (Mllbb/1e3>=450 && Mllbb/1e3<1000 && DeltaEta>1.5) || (Mllbb/1e3>=1000 && Mllbb/1e3<1500 && DeltaEta>2) || (Mllbb/1e3>=1500 && Mllbb/1e3<2500 && DeltaEta>2.5))"],
        # ["VR2","(DeltaPhi/TMath::Pi())>0.5 && (DeltaPhi/TMath::Pi())<=0.8  && ((Mllbb/1e3<450 && DeltaEta>1) || (Mllbb/1e3>=450 && Mllbb/1e3<1000 && DeltaEta>1.5) || (Mllbb/1e3>=1000 && Mllbb/1e3<1500 && DeltaEta>2) || (Mllbb/1e3>=1500 && Mllbb/1e3<2500 && DeltaEta>2.5))"],
        ["VR3","(DeltaPhi/TMath::Pi())>0.8 && (DeltaPhi/TMath::Pi())<=0.9  && ((Mllbb/1e3<450 && DeltaEta>1) || (Mllbb/1e3>=450 && Mllbb/1e3<1000 && DeltaEta>1.5) || (Mllbb/1e3>=1000 && Mllbb/1e3<1500 && DeltaEta>2) || (Mllbb/1e3>=1500 && Mllbb/1e3<2500 && DeltaEta>2.5))"],
        # ["VR4","(DeltaPhi/TMath::Pi())>0.9 && (DeltaPhi/TMath::Pi())<=0.95 && ((Mllbb/1e3<450 && DeltaEta>1) || (Mllbb/1e3>=450 && Mllbb/1e3<1000 && DeltaEta>1.5) || (Mllbb/1e3>=1000 && Mllbb/1e3<1500 && DeltaEta>2) || (Mllbb/1e3>=1500 && Mllbb/1e3<2500 && DeltaEta>2.5))"],
        # ["VR5","(DeltaPhi/TMath::Pi())>0.95 && (DeltaPhi/TMath::Pi())<=1.0 && ((Mllbb/1e3<450 && DeltaEta>1) || (Mllbb/1e3>=450 && Mllbb/1e3<1000 && DeltaEta>1.5) || (Mllbb/1e3>=1000 && Mllbb/1e3<1500 && DeltaEta>2) || (Mllbb/1e3>=1500 && Mllbb/1e3<2500 && DeltaEta>2.5))"],
    ],
    
    # weights to be combined into one - will be called weight
    # NB: for sampleType "data", weight set to "1"
    "weight": "weight_normalise*weight_leptonSF*weight_bTagSF_DL1r_77*weight_pileup*weight_jvt*weight_mc*(0.259712*(runNumber==284500) + 0.318705*(runNumber==300000) + 0.421583*(runNumber==310000))",
    "weight_ttbar": "",
    "weight_ttbar_alt": "",
    "weight_bkg": "",
    "weight_bkg_alt": "",
    "weight_signal": "",
    
    # new variables to be created - a list of lists with two entries; 1) name of new variable, 2) how it is constructed
    "new_vars": [
        #["channel", "((el_pt.size()==2)*0 + (el_pt.size()==0)*1 + (el_pt.size()==1)*2"],
        ["ee","(el_pt.size()==2)"],
        ["mumu","(mu_pt.size()==2)"],
        ["emu","(el_pt.size()==1 && mu_pt.size()==1)"],
        ["lep1_flav","( (el_1_pt==lep1_pt)*11 + (mu_1_pt==lep1_pt)*13 )"],
        ["lep2_flav","( (el_2_pt==lep2_pt)*11 + (mu_2_pt==lep2_pt)*13 )"],
        ["jet1_pt","jet_pt[0]"],
        ["jet2_pt","jet_pt[1]"],
    ],
    "new_vars_ttbar": [
        ["isFake_el", "(el_pt.size()==2 || (el_pt.size()==1 && ((el_pt[0]>mu_pt[0] && !lep1_isReal) || (el_pt[0]<mu_pt[0] && !lep2_isReal))))"],
        ["isFake_mu", "(mu_pt.size()==2 || (mu_pt.size()==1 && ((mu_pt[0]>el_pt[0] && !lep1_isReal) || (mu_pt[0]<el_pt[0] && !lep2_isReal))))"],
    ],
    "new_vars_ttbar_alt": [
        ["isFake_el", "(el_pt.size()==2 || (el_pt.size()==1 && ((el_pt[0]>mu_pt[0] && !lep1_isReal) || (el_pt[0]<mu_pt[0] && !lep2_isReal))))"],
        ["isFake_mu", "(mu_pt.size()==2 || (mu_pt.size()==1 && ((mu_pt[0]>el_pt[0] && !lep1_isReal) || (mu_pt[0]<el_pt[0] && !lep2_isReal))))"],
    ],
    "new_vars_bkg": [
        ["isFake_el", "(el_pt.size()==2 || (el_pt.size()==1 && ((el_pt[0]>mu_pt[0] && !lep1_isReal) || (el_pt[0]<mu_pt[0] && !lep2_isReal))))"],
        ["isFake_mu", "(mu_pt.size()==2 || (mu_pt.size()==1 && ((mu_pt[0]>el_pt[0] && !lep1_isReal) || (mu_pt[0]<el_pt[0] && !lep2_isReal))))"],
    ],
    "new_vars_bkg_alt": [
        ["isFake_el", "(el_pt.size()==2 || (el_pt.size()==1 && ((el_pt[0]>mu_pt[0] && !lep1_isReal) || (el_pt[0]<mu_pt[0] && !lep2_isReal))))"],
        ["isFake_mu", "(mu_pt.size()==2 || (mu_pt.size()==1 && ((mu_pt[0]>el_pt[0] && !lep1_isReal) || (mu_pt[0]<el_pt[0] && !lep2_isReal))))"],
    ],
    "new_vars_signal": [
        ["isFake_el", "(el_pt.size()==2 || (el_pt.size()==1 && ((el_pt[0]>mu_pt[0] && !lep1_isReal) || (el_pt[0]<mu_pt[0] && !lep2_isReal))))"],
        ["isFake_mu", "(mu_pt.size()==2 || (mu_pt.size()==1 && ((mu_pt[0]>el_pt[0] && !lep1_isReal) || (mu_pt[0]<el_pt[0] && !lep2_isReal))))"],
    ],
    
    # new syst weight variables
    "new_vars_weights": [
        ["weight_muFup",   "SystWeight(mc_generator_weights[1],mc_generator_weights[0])"],
        ["weight_muFdown", "SystWeight(mc_generator_weights[2],mc_generator_weights[0])"],
        ["weight_muRup",   "SystWeight(mc_generator_weights[3],mc_generator_weights[0])"],
        ["weight_muRdown", "SystWeight(mc_generator_weights[4],mc_generator_weights[0])"],
        
        ["weight_a14_up",  "SystWeight(mc_generator_weights[193],mc_generator_weights[0])"],
        ["weight_a14_down","SystWeight(mc_generator_weights[194],mc_generator_weights[0])"],
        ["weight_FSRup",   "SystWeight(mc_generator_weights[198],mc_generator_weights[0])"],
        ["weight_FSRdown", "SystWeight(mc_generator_weights[199],mc_generator_weights[0])"],
        
        ["weight_pdf0",  "SystWeight(mc_generator_weights[115],mc_generator_weights[11])"],
        ["weight_pdf1",  "SystWeight(mc_generator_weights[116],mc_generator_weights[11])"],
        ["weight_pdf2",  "SystWeight(mc_generator_weights[117],mc_generator_weights[11])"],
        ["weight_pdf3",  "SystWeight(mc_generator_weights[118],mc_generator_weights[11])"],
        ["weight_pdf4",  "SystWeight(mc_generator_weights[119],mc_generator_weights[11])"],
        ["weight_pdf5",  "SystWeight(mc_generator_weights[120],mc_generator_weights[11])"],
        ["weight_pdf6",  "SystWeight(mc_generator_weights[121],mc_generator_weights[11])"],
        ["weight_pdf7",  "SystWeight(mc_generator_weights[122],mc_generator_weights[11])"],
        ["weight_pdf8",  "SystWeight(mc_generator_weights[123],mc_generator_weights[11])"],
        ["weight_pdf9",  "SystWeight(mc_generator_weights[124],mc_generator_weights[11])"],
        ["weight_pdf10", "SystWeight(mc_generator_weights[125],mc_generator_weights[11])"],
        ["weight_pdf11", "SystWeight(mc_generator_weights[126],mc_generator_weights[11])"],
        ["weight_pdf12", "SystWeight(mc_generator_weights[127],mc_generator_weights[11])"],
        ["weight_pdf13", "SystWeight(mc_generator_weights[128],mc_generator_weights[11])"],
        ["weight_pdf14", "SystWeight(mc_generator_weights[129],mc_generator_weights[11])"],
        ["weight_pdf15", "SystWeight(mc_generator_weights[130],mc_generator_weights[11])"],
        ["weight_pdf16", "SystWeight(mc_generator_weights[131],mc_generator_weights[11])"],
        ["weight_pdf17", "SystWeight(mc_generator_weights[132],mc_generator_weights[11])"],
        ["weight_pdf18", "SystWeight(mc_generator_weights[133],mc_generator_weights[11])"],
        ["weight_pdf19", "SystWeight(mc_generator_weights[134],mc_generator_weights[11])"],
        ["weight_pdf20", "SystWeight(mc_generator_weights[135],mc_generator_weights[11])"],
        ["weight_pdf21", "SystWeight(mc_generator_weights[136],mc_generator_weights[11])"],
        ["weight_pdf22", "SystWeight(mc_generator_weights[137],mc_generator_weights[11])"],
        ["weight_pdf23", "SystWeight(mc_generator_weights[138],mc_generator_weights[11])"],
        ["weight_pdf24", "SystWeight(mc_generator_weights[139],mc_generator_weights[11])"],
        ["weight_pdf25", "SystWeight(mc_generator_weights[140],mc_generator_weights[11])"],
        ["weight_pdf26", "SystWeight(mc_generator_weights[141],mc_generator_weights[11])"],
        ["weight_pdf27", "SystWeight(mc_generator_weights[142],mc_generator_weights[11])"],
        ["weight_pdf28", "SystWeight(mc_generator_weights[143],mc_generator_weights[11])"],
        ["weight_pdf29", "SystWeight(mc_generator_weights[144],mc_generator_weights[11])"],
        
        ["weight_full_pileup_up",   "weight_normalise*weight_leptonSF*weight_bTagSF_DL1r_77*weight_pileup_UP*weight_jvt*weight_mc*(0.259712*(runNumber==284500) + 0.318705*(runNumber==300000) + 0.421583*(runNumber==310000))"],
        ["weight_full_pileup_down", "weight_normalise*weight_leptonSF*weight_bTagSF_DL1r_77*weight_pileup_DOWN*weight_jvt*weight_mc*(0.259712*(runNumber==284500) + 0.318705*(runNumber==300000) + 0.421583*(runNumber==310000))"],
        
        ["weight_pileup_up",   "SystWeight(weight_pileup_UP,weight_pileup)"],
        ["weight_pileup_down", "SystWeight(weight_pileup_DOWN,weight_pileup)"],
        ["weight_jvt_up",   "SystWeight(weight_jvt_UP,weight_jvt)"],
        ["weight_jvt_down", "SystWeight(weight_jvt_DOWN,weight_jvt)"],

        ["weight_elTrigger_up",   "SystWeight(weight_leptonSF_EL_SF_Trigger_UP,weight_leptonSF)"],
        ["weight_elTrigger_down", "SystWeight(weight_leptonSF_EL_SF_Trigger_DOWN,weight_leptonSF)"],
        ["weight_elReco_up",      "SystWeight(weight_leptonSF_EL_SF_Reco_UP,weight_leptonSF)"],
        ["weight_elReco_down",    "SystWeight(weight_leptonSF_EL_SF_Reco_DOWN,weight_leptonSF)"],
        ["weight_elId_up",        "SystWeight(weight_leptonSF_EL_SF_ID_UP,weight_leptonSF)"],
        ["weight_elId_down",      "SystWeight(weight_leptonSF_EL_SF_ID_DOWN,weight_leptonSF)"],
        ["weight_elIso_up",       "SystWeight(weight_leptonSF_EL_SF_Isol_UP,weight_leptonSF)"],
        ["weight_elIso_down",     "SystWeight(weight_leptonSF_EL_SF_Isol_DOWN,weight_leptonSF)"],

        ["weight_muTrigger_stat_up",   "SystWeight(weight_leptonSF_MU_SF_Trigger_STAT_UP,weight_leptonSF)"],
        ["weight_muTrigger_stat_down", "SystWeight(weight_leptonSF_MU_SF_Trigger_STAT_DOWN,weight_leptonSF)"],
        ["weight_muTTVA_stat_up",      "SystWeight(weight_leptonSF_MU_SF_TTVA_STAT_UP,weight_leptonSF)"],
        ["weight_muTTVA_stat_down",    "SystWeight(weight_leptonSF_MU_SF_TTVA_STAT_DOWN,weight_leptonSF)"],
        ["weight_muId_stat_up",        "SystWeight(weight_leptonSF_MU_SF_ID_STAT_UP,weight_leptonSF)"],
        ["weight_muId_stat_down",      "SystWeight(weight_leptonSF_MU_SF_ID_STAT_DOWN,weight_leptonSF)"],
        ["weight_muIso_stat_up",       "SystWeight(weight_leptonSF_MU_SF_Isol_STAT_UP,weight_leptonSF)"],
        ["weight_muIso_stat_down",     "SystWeight(weight_leptonSF_MU_SF_Isol_STAT_DOWN,weight_leptonSF)"],

        ["weight_muTrigger_syst_up",   "SystWeight(weight_leptonSF_MU_SF_Trigger_SYST_UP,weight_leptonSF)"],
        ["weight_muTrigger_syst_down", "SystWeight(weight_leptonSF_MU_SF_Trigger_SYST_DOWN,weight_leptonSF)"],
        ["weight_muTTVA_syst_up",      "SystWeight(weight_leptonSF_MU_SF_TTVA_SYST_UP,weight_leptonSF)"],
        ["weight_muTTVA_syst_down",    "SystWeight(weight_leptonSF_MU_SF_TTVA_SYST_DOWN,weight_leptonSF)"],
        ["weight_muId_syst_up",        "SystWeight(weight_leptonSF_MU_SF_ID_SYST_UP,weight_leptonSF)"],
        ["weight_muId_syst_down",      "SystWeight(weight_leptonSF_MU_SF_ID_SYST_DOWN,weight_leptonSF)"],
        ["weight_muIso_syst_up",       "SystWeight(weight_leptonSF_MU_SF_Isol_SYST_UP,weight_leptonSF)"],
        ["weight_muIso_syst_down",     "SystWeight(weight_leptonSF_MU_SF_Isol_SYST_DOWN,weight_leptonSF)"],
        
        ["has_1elCloseToJet", "( (el_pt.size()==1 && ( (lep1_dRjetMin<0.4 && el_charge[0]>0) || (lep2_dRjetMin<0.4 && el_charge[0]<0) ) ) || (el_pt.size()==2 && ( lep1_dRjetMin<0.4 || lep2_dRjetMin<0.4 ) ) )" ],
        ["has_2elCloseToJet", "( (el_pt.size()==2 && ( lep1_dRjetMin<0.4 && lep2_dRjetMin<0.4 ) ) )" ],
        
        ["weight_btag_b0_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[0],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b1_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[1],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b2_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[2],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b3_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[3],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b4_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[4],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b5_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[5],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b6_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[6],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b7_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[7],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b8_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_up[8],weight_bTagSF_DL1r_77)"],
        ["weight_btag_c0_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_C_up[0],weight_bTagSF_DL1r_77)"],
        ["weight_btag_c1_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_C_up[1],weight_bTagSF_DL1r_77)"],
        ["weight_btag_c2_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_C_up[2],weight_bTagSF_DL1r_77)"],
        ["weight_btag_c3_up",        "SystWeight(weight_bTagSF_DL1r_77_eigenvars_C_up[3],weight_bTagSF_DL1r_77)"],
        ["weight_btag_light0_up",    "SystWeight(weight_bTagSF_DL1r_77_eigenvars_Light_up[0],weight_bTagSF_DL1r_77)"],
        ["weight_btag_light1_up",    "SystWeight(weight_bTagSF_DL1r_77_eigenvars_Light_up[1],weight_bTagSF_DL1r_77)"],
        ["weight_btag_light2_up",    "SystWeight(weight_bTagSF_DL1r_77_eigenvars_Light_up[2],weight_bTagSF_DL1r_77)"],
        ["weight_btag_light3_up",    "SystWeight(weight_bTagSF_DL1r_77_eigenvars_Light_up[3],weight_bTagSF_DL1r_77)"],
        ["weight_btag_extrap_up",    "SystWeight(weight_bTagSF_DL1r_77_extrapolation_up,weight_bTagSF_DL1r_77)"],
        ["weight_btag_extrap_c_up",  "SystWeight(weight_bTagSF_DL1r_77_extrapolation_from_charm_up,weight_bTagSF_DL1r_77)"],
        ["weight_btag_b0_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[0],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b1_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[1],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b2_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[2],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b3_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[3],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b4_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[4],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b5_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[5],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b6_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[6],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b7_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[7],weight_bTagSF_DL1r_77)"],
        ["weight_btag_b8_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_B_down[8],weight_bTagSF_DL1r_77)"],
        ["weight_btag_c0_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_C_down[0],weight_bTagSF_DL1r_77)"],
        ["weight_btag_c1_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_C_down[1],weight_bTagSF_DL1r_77)"],
        ["weight_btag_c2_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_C_down[2],weight_bTagSF_DL1r_77)"],
        ["weight_btag_c3_down",      "SystWeight(weight_bTagSF_DL1r_77_eigenvars_C_down[3],weight_bTagSF_DL1r_77)"],
        ["weight_btag_light0_down",  "SystWeight(weight_bTagSF_DL1r_77_eigenvars_Light_down[0],weight_bTagSF_DL1r_77)"],
        ["weight_btag_light1_down",  "SystWeight(weight_bTagSF_DL1r_77_eigenvars_Light_down[1],weight_bTagSF_DL1r_77)"],
        ["weight_btag_light2_down",  "SystWeight(weight_bTagSF_DL1r_77_eigenvars_Light_down[2],weight_bTagSF_DL1r_77)"],
        ["weight_btag_light3_down",  "SystWeight(weight_bTagSF_DL1r_77_eigenvars_Light_down[3],weight_bTagSF_DL1r_77)"],
        ["weight_btag_extrap_down",  "SystWeight(weight_bTagSF_DL1r_77_extrapolation_down,weight_bTagSF_DL1r_77)"],
        ["weight_btag_extrap_c_down","SystWeight(weight_bTagSF_DL1r_77_extrapolation_from_charm_down,weight_bTagSF_DL1r_77)"],    
    ],
    
    # any ROOT macros which are required for cuts/weights/new variables
    "functions": [  
        "macros/SystWeight.C",
        #"macros/C_llbb.C",
    ],
}

