# Example script to create all slimmed ntuple inputs

# -- where to run:
batch="local"
# batch="csub"

# -- RDF options (for local jobs use MT=yes and lazy=no; for batch MT=no, lazy=yes):
MT="yes"
lazy="no"
# MT="no"
# lazy="yes"

# -- input directories
# nominalPath="/eos/infnts/atlas/public/ttRes_L2ntuples/V25-nominal-Zprime/"
# systPath="/eos/infnts/atlas/public/ttRes_L2ntuples/V25-syst-Zprime/*"
nominalPath="/eos/infnts/atlas/public/ttRes_L2ntuples/V25-nominal-RDataFrame/"
systPath="/eos/infnts/atlas/public/ttRes_L2ntuples/V25-syst-RDataFrame/*/"

# -- output path
# outPath="/eos/infnts/atlas/public/ttRes_slimNtups/V25-Zprime/"
outPath="/eos/infnts/atlas/public/ttRes_slimNtups/V25-RDataFrame/"
# outPath="/eos/infnts/atlas/public/ttRes_slimNtups/V25-RDataFrame-tmp/"

# config="ntuple_slimmer_config"
config="ntuple_slimmer_config_VR"

# ................................

# -- list of nominal ttbar samples
ttbarSamples=""
# ttbarSamples+=" ttbar_dil"

# -- list of other background samples
bkgSamples=""
# bkgSamples+=" sintop_wt_dilep zjets diboson ttV ttH"
bkgSamples+=" sintop_wt_dilep"

# -- list of signal samples
sigSamples=""
# sigSamples+=" zprimeTC2_400 zprimeTC2_500 zprimeTC2_750 zprimeTC2_1000 zprimeTC2_1250 zprimeTC2_1500 zprimeTC2_1750 zprimeTC2_2000 zprimeTC2_2500 zprimeTC2_3000 zprimeTC2_3500 zprimeTC2_4000 zprimeTC2_4500 zprimeTC2_5000 zprimeTC2_6000"
# sigSamples+=" kkg500MG kkg1000MG kkg1500MG kkg2000MG kkg2500MG kkg3000MG kkg3500MG kkg4000MG kkg4500MG kkg5000MG"
# sigSamples+=" grav400 grav500 grav750 grav1000 grav2000 grav3000"

# -- list of alternative ttbar samples
altTtbarSamples=""
# altTtbarSamples+=" ttbar_nah"
# altTtbarSamples+=" ttbar_dil_PH7 ttbar_dil_aMCP8 ttbar_dil_aMCH7 ttbar_dil_hdamp ttbar_dil_MECoff"
# altTtbarSamples+=" ttbar_dil_pThard1"

# -- list of alternative non-ttbar samples
altBkgSamples=""
# altBkgSamples+=" sintop_wt_dilep_PH7 sintop_wt_dilep_aMCP8 sintop_wt_dilep_DS"
# altBkgSamples+=" wjets sintop"

# -- data samples
dataSamples=""
### dataSamples+=" data15_FullRun2 data16_FullRun2 data17_FullRun2 data18_FullRun2"
# dataSamples+=" data15 data16 data17 data18"

# ................................

# -- channel directories
# channelDirs="ee_ttbar_dilep mumu_ttbar_dilep emu_ttbar_dilep" # old Z' ntuples
channelDirs="mca mcd mce" # RDF-based HeavyHiggs ones

# -- MC campaigns - doesn't apply to data
mcSuffs="mc*"

# -- list of all systematic trees
systematics=""
systematics+=" nominal"
# systematics+=" "`cat systTrees.txt`
# systematics+=" "`cat systTrees_test.txt`
# systematics+=" "`cat systTree_V25.txt`
# systematics+=" "`cat systTree_LatestJETRecommendations_V25.txt`
# systematics+=" EG_SCALE_ALL__1up JET_EffectiveNP_Mixed1__1down JET_EffectiveNP_Mixed3__1up JET_EtaIntercalibration_NonClosure_negEta__1down JET_Flavor_Composition__1down JET_Flavor_Response__1down JET_Flavour_PerJet_GenShower_HF__1up JET_Flavour_PerJet_Shower_HF__1up JET_JER_EffectiveNP_2__1up JET_JER_EffectiveNP_5__1up JET_JER_EffectiveNP_7__1down_PseudoData JET_JER_EffectiveNP_8__1down JET_JER_EffectiveNP_9__1down MUON_SCALE__1down"
# systematics+=" JET_Flavor_Composition_prop__1up JET_Flavor_Composition_prop__1down JET_Flavor_Response_prop__1up JET_Flavor_Response_prop__1down JET_Flavour_PerJet_GenShower_HF__1up JET_Flavour_PerJet_GenShower_HF__1down JET_Flavour_PerJet_GenShower__1up JET_Flavour_PerJet_GenShower__1down JET_Flavour_PerJet_Hadronization_HF__1up JET_Flavour_PerJet_Hadronization_HF__1down JET_Flavour_PerJet_Hadronization__1up JET_Flavour_PerJet_Hadronization__1down JET_Flavour_PerJet_Shower_HF__1up JET_Flavour_PerJet_Shower_HF__1down JET_Flavour_PerJet_Shower__1up JET_Flavour_PerJet_Shower__1down"


# -- FINISH CONFIG --

mkdir -p ${outPath}

# -- declaring function to do the hard work
function process_events () {
    if [[ "${specOpt}" != *"--ht-submitonly"* ]]
    then
        if [[ $jobIdx > 0 ]]
        then
            specOpt=" --ht-update"
        else
            specOpt=""
        fi
        specOpt+=" --ht-nosubmit"
        jobIdx=`expr ${jobIdx} + 1`
    fi
    # create input file list:
    inFiles=""
    for ch in ${channelDirs}
    do
        if [[ ${mcSuffs} != "" ]]
        then
            for mc in ${mcSuffs}
            do
                if [[ $inFiles != "" ]]; then inFiles+=","; fi
                inFiles+=${inPath}/${ch}/${smp}_${mc}${suf}.root
            done
        else
            if [[ $inFiles != "" ]]; then inFiles+=","; fi
            inFiles+=${inPath}/${ch}/${smp}${suf}.root
        fi
    done
    
    # single output file for all input files:
    outFile=${smp}${suf}.root
    
    # set options to be passed to the job:
    options=" -t ${syst} -f ${inFiles} -o ${outFile} -p ${outPath} -c ${config} -s ${sample}"
    if [[ $MT == "no" ]]; then options+=" -m 0"; fi
    if [[ $lazy == "no" ]]; then options+=" -l 0"; fi
    export OPT=$options
    if [[ $batch == "local" ]]
    then
        echo ""
        echo "...................................................."
        echo "Running: python ntuple_slimmer.py ${OPT}"
        echo ""
        eval python ntuple_slimmer.py ${OPT}
        echo ""
    else
        eval ${batch} ttResDilepSlim_${sample} ./ntuple_slimmer.job ${OPT} ${specOpt}
    fi
}


# --- run on all samples

# ttbar
jobIdx=`expr 0`
specOpt=""
for smp in $ttbarSamples
do
    for syst in ${systematics}
    do
        sample="ttbar"
        suf=""
        inPath=${nominalPath}
        if [[ $syst != "nominal" ]]
        then
            sample="ttbar_alt"
            suf="__${syst}"
            inPath=${systPath}
        fi
        process_events
    done
done
if [[ $batch == "csub" && $ttbarSamples != "" ]]
then
    specOpt=" --ht-submitonly"
    process_events
fi

# bkg
jobIdx=`expr 0`
specOpt=""
for smp in $bkgSamples
do
    for syst in ${systematics}
    do
        sample="bkg"
        suf=""
        inPath=${nominalPath}
        if [[ $syst != "nominal" ]]
        then
            sample="bkg_alt"
            suf="__${syst}"
            inPath=${systPath}
        fi
        process_events
    done
done
if [[ $batch == "csub" && $bkgSamples != "" ]]
then
    specOpt=" --ht-submitonly"
    process_events
fi

sample="ttbar_alt"
inPath=${nominalPath}
syst="nominal"
suf=""
jobIdx=`expr 0`
specOpt=""
for smp in $altTtbarSamples
do
    for syst in ${systematics}
    do
        if [[ ${syst} == "nominal" ]]
        then
            process_events
        fi
    done
done
if [[ $batch == "csub" && $altTtbarSamples != "" ]]
then
    specOpt=" --ht-submitonly"
    process_events
fi

sample="bkg_alt"
inPath=${nominalPath}
syst="nominal"
suf=""
jobIdx=`expr 0`
specOpt=""
for smp in $altBkgSamples
do
    for syst in ${systematics}
    do
        if [[ ${syst} == "nominal" ]]
        then
            process_events
        fi
    done
done
if [[ $batch == "csub" && $altBkgSamples != "" ]]
then
    specOpt=" --ht-submitonly"
    process_events
fi

sample="signal"
inPath=${nominalPath}
syst="nominal"
suf=""
jobIdx=`expr 0`
specOpt=""
for smp in $sigSamples
do
    for syst in ${systematics}
    do
        if [[ ${syst} == "nominal" ]]
        then
            process_events
        fi
    done
done
if [[ $batch == "csub" && $sigSamples != "" ]]
then
    specOpt=" --ht-submitonly"
    process_events
fi

sample="data"
inPath=${nominalPath}
syst="nominal"
suf=""
mcSuffs=""
jobIdx=`expr 0`
specOpt=""
for smp in $dataSamples
do
    for syst in ${systematics}
    do
        if [[ ${syst} == "nominal" ]]
        then
            process_events
        fi
    done
done
if [[ $batch == "csub" && $dataSamples != "" ]]
then
    specOpt=" --ht-submitonly"
    process_events
fi
