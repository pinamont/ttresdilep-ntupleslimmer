#!bin/sh

# Setup ROOT and gcc
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
# lsetup "views LCG_102b_ATLAS_2 x86_64-centos7-gcc11-opt" --quiet
# localSetupROOT 6.28.00-x86_64-centos7-gcc11-opt --quiet
# lsetup "views LCG_102rc1_ATLAS_14 x86_64-centos7-gcc11-opt" --quiet
lsetup "views LCG_102b_ATLAS_12 x86_64-centos7-gcc11-opt" --quiet

source ../Test/ht_condor_setup.sh
