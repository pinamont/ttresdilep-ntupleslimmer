# ttResDilep-NtupleSlimmer

Now with multi-thread enabled! Super fast!

## Setup

```
git clone ssh://git@gitlab.cern.ch:7999/pinamont/ttresdilep-ntupleslimmer.git ttResDilep-NtupleSlimmer
cd ttResDilep-NtupleSlimmer
source setup.sh
```

# Test run

```
python ntuple_slimmer.py -t nominal -f INPUT-FILE -o OUTPUT-FILE -c ntuple_slimmer_config.py
```

# Options

  * `-s` sample type ("data", "ttbar", "ttbar_alt", "bkg", "bkg_alt", "signal") - default is "ttbar"
  * `-e` max number of events to run on
  * `-d` debug level
  * `-m` multi-thread (default is ON, can switch it OFF by setting this to 0)

# Production run

To update.

<!-- * Modify and run script `run_slimmer.sh` -->
